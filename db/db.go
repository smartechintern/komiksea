package db

import (
	"komiksea/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func Database(connString string) gin.HandlerFunc {
	db, err := gorm.Open("mysql", connString)
	db.LogMode(true)
	// Error
	if err != nil {
		panic(err)
	}

	// userTableName := db.NewScope(&models.User{}).GetModelStruct().TableName(db)

	// // tbl_user
	// db.AutoMigrate(&models.User{})
	// db.Model(&models.User{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.User{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.User{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.User{}).AddIndex("idx_user_name", "name")
	// db.Model(&models.User{}).AddIndex("idx_user_email", "email")
	// db.Model(&models.User{}).AddIndex("idx_user_email_password", "email", "password")

	// // tbl_artist
	// db.AutoMigrate(&models.Artist{})
	// db.Model(&models.Artist{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Artist{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Artist{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Artist{}).AddIndex("idx_tbl_artist_name", "name")

	// // tbl_author
	// db.AutoMigrate(&models.Author{})
	// db.Model(&models.Author{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Author{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Author{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Author{}).AddIndex("idx_tbl_author_name", "name")

	// // tbl_comics
	// db.AutoMigrate(&models.Comics{})
	// db.Model(&models.Comics{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Comics{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Comics{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Comics{}).AddIndex("idx_tbl_comics_name", "name")
	// db.Model(&models.Comics{}).AddIndex("idx_tbl_comics_otherName", "otherName")
	// db.Model(&models.Comics{}).AddIndex("idx_tbl_comics_authorId", "authorId")
	// db.Model(&models.Comics{}).AddIndex("idx_tbl_comics_artistId", "artistId")

	// // tbl_comics_pages
	// db.AutoMigrate(&models.ComicsPages{})
	// db.Model(&models.ComicsPages{}).AddForeignKey("comicsId", "tbl_comics"+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsPages{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsPages{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsPages{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsPages{}).AddIndex("idx_tbl_comics_pages_comicsId", "comicsId")

	// // tbl_category
	// db.AutoMigrate(&models.Category{})
	// db.Model(&models.Category{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Category{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Category{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.Category{}).AddIndex("idx_tbl_category_name", "name")

	// // tbl_comics_category
	// db.AutoMigrate(&models.ComicsCategory{})
	// db.Model(&models.ComicsCategory{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsCategory{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsCategory{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsCategory{}).AddForeignKey("comicsId", "tbl_comics"+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsCategory{}).AddForeignKey("categoryId", "tbl_category"+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsCategory{}).AddIndex("idx_tbl_comics_category_comicsId", "comicsId")
	// db.Model(&models.ComicsCategory{}).AddIndex("idx_tbl_comics_category_categoryId", "categoryId")

	// // tbl_comics_views
	// db.AutoMigrate(&models.ComicsViews{})
	// db.Model(&models.ComicsViews{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsViews{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsViews{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsViews{}).AddForeignKey("pagesId", "tbl_comics_pages"+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsViews{}).AddIndex("idx_tbl_comics_views_pagesId", "pagesId")

	// // tbl_comics_rating
	// db.AutoMigrate(&models.ComicsRating{})
	// db.Model(&models.ComicsRating{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsRating{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsRating{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsRating{}).AddForeignKey("comicsId", "tbl_comics"+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.ComicsRating{}).AddIndex("idx_tbl_comics_rating_comicsId", "comicsId")
	// db.Model(&models.ComicsRating{}).AddIndex("idx_tbl_comics_rating_rating", "rating")

	// // tbl_user_comics
	// db.AutoMigrate(&models.UserComics{})
	// db.Model(&models.UserComics{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserComics{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserComics{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserComics{}).AddForeignKey("comicsId", "tbl_comics"+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserComics{}).AddIndex("idx_tbl_user_comics_comicsId", "comicsId")

	// // tbl_user_bookmarks
	// db.AutoMigrate(&models.UserBookmarks{})
	// db.Model(&models.UserBookmarks{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserBookmarks{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserBookmarks{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserBookmarks{}).AddForeignKey("comicsId", "tbl_comics" +"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserBookmarks{}).AddIndex("idx_tbl_user_bookmarks_comicsId", "comicsId")

	// // tbl_user_bookmarks
	// db.AutoMigrate(&models.UserHistory{})
	// db.Model(&models.UserHistory{}).AddForeignKey("createdBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserHistory{}).AddForeignKey("updatedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserHistory{}).AddForeignKey("deletedBy", userTableName+"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserHistory{}).AddForeignKey("comicsId", "tbl_comics" +"(id)", "RESTRICT", "RESTRICT")
	// db.Model(&models.UserHistory{}).AddIndex("idx_tbl_user_history_comicsId", "comicsId")

	return func(c *gin.Context) {
		c.Set("DB", db)
		c.Next()
	}
}
