package controllers

import (
    "komiksea/models"

    "github.com/biezhi/gorm-paginator/pagination"
    "github.com/gin-gonic/gin"
    "github.com/jinzhu/gorm"
    "strconv"
)

func AddTodo(c *gin.Context) {

    db := c.MustGet("DB").(*gorm.DB)
    
    var todo models.User
    if c.BindJSON(&todo) != nil {
        c.JSON(406, gin.H{"message": "Invalid data", "data": todo})
        c.Abort()
        return
    }

    if !db.NewRecord(todo) {
        c.JSON(406, gin.H{"message": "Todo could not be created"})
        c.Abort()
        return
        
    }
    db.Create(&todo)
    c.JSON(200, gin.H{"message": "Todo created"})
}

func DeleteTodo(c *gin.Context) {
    db := c.MustGet("DB").(*gorm.DB)
    id := c.Param("id")
    var todo models.User
    db.First(&todo, id)
    db.Delete(&todo)
    c.JSON(200, gin.H{"message": "ok"})
}

func ListUser(c *gin.Context) {
    page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
    limit, _ := strconv.Atoi(c.DefaultQuery("limit", "10"))

    db := c.MustGet("DB").(*gorm.DB)
    users := []models.User{}
    paginator := pagination.Pagging(&pagination.Param{
        DB:      db,
        Page:    page,
        Limit:   limit,
        OrderBy: []string{"id desc"},
        ShowSQL: true,
    }, &users)

    c.JSON(200, paginator)


    //db.Find(&users)
    //c.JSON(200, gin.H{"success": true, "total": len(users), "users": &users})
}