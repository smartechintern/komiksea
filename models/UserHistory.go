package models

type UserHistory struct {
	//CreatedBy User `gorm:"column:createdBy; not null"`
	//UpdatedBy User `gorm:"column:updatedBy; null"`
	//DeletedBy User `gorm:"column:deletedBy; null"`
	ID
	ComicsID    uint   `gorm:"column:comicsId; not null"`
	Model
}

func (UserHistory) TableName() string {
	return "tbl_user_history"
}
