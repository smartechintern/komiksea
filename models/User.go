package models

type User struct {
	ID
	Name          string `gorm:"size:191; column:name; not null" json:"name"`
	Email         string `gorm:"unique; size:191; column:email; not null" json:"email"`
	Avatar        string `gorm:"size:191; column:avatar; default: 'users/default.png'" json:"avatar"`
	Password      []byte `gorm:"column:password; not null" json:"password"`
	RememberToken string `gorm:"size:191; column:remember_token; default: 'users/default.png'" json:"remember_token"`
	UserModel
}

func (User) TableName() string {
	return "tbl_user"
}
