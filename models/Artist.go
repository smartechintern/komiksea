package models

import "time"

type Artist struct {
	//CreatedBy User `gorm:"column:createdBy; not null"`
	//UpdatedBy User `gorm:"column:updatedBy; null"`
	//DeletedBy User `gorm:"column:deletedBy; null"`
	ID
	Image     string    `gorm:"size:191; column:image"`
	Name      string    `gorm:"size:191; column:name; not null"`
	BirthDate time.Time `gorm:"size:191; column:birthDate" json:",string"`
	Model
}

func (Artist) TableName() string {
	return "tbl_artist"
}
