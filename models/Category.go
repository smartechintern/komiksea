package models

type Category struct {
	//CreatedBy User `gorm:"column:createdBy; not null"`
	//UpdatedBy User `gorm:"column:updatedBy; null"`
	//DeletedBy User `gorm:"column:deletedBy; null"`
	ID
	Name string `gorm:"size:191; column:name; not null"`
	Model
}

func (Category) TableName() string {
	return "tbl_category"
}
