package models

import "time"

type Comics struct {
	//CreatedBy User `gorm:"column:createdBy; not null"`
	//UpdatedBy User `gorm:"column:updatedBy; null"`
	//DeletedBy User `gorm:"column:deletedBy; null"`
	ID
	Image        string    `gorm:"size:191; column:image"`
	Name         string    `gorm:"unique; size:191; column:name; not null"`
	OtherName    string    `gorm:"size:191; column:otherName"`
	BirthDate    time.Time `gorm:"column:birthDate" json:",string"`
	AuthorID     uint      `gorm:"column:authorId; not null"`
	ArtistID     uint      `gorm:"column:artistId; not null"`
	ReleasedYear time.Time `gorm:"column:releasedYear"`
	Model
}

func (Comics) TableName() string {
	return "tbl_comics"
}
