package models

type ComicsViews struct {
	//CreatedBy User `gorm:"column:createdBy; not null"`
	//UpdatedBy User `gorm:"column:updatedBy; null"`
	//DeletedBy User `gorm:"column:deletedBy; null"`
	ID
	PagesID      uint      `gorm:"column:pagesId; not null"`
	Model
}

func (ComicsViews) TableName() string {
	return "tbl_comics_views"
}
