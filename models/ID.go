package models

type ID struct {
	ID uint `gorm:"primary_key; not null; column:id" json:"id"`
}
