package models

type ComicsCategory struct {
	//CreatedBy User `gorm:"column:createdBy; not null"`
	//UpdatedBy User `gorm:"column:updatedBy; null"`
	//DeletedBy User `gorm:"column:deletedBy; null"`
	ID
	ComicsID   uint `gorm:"column:comicsId; not null"`
	CategoryID uint `gorm:"column:categoryId; not null"`
	Model
}

func (ComicsCategory) TableName() string {
	return "tbl_comics_category"
}
