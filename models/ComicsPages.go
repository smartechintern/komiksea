package models

import "time"

type ComicsPages struct {
	//CreatedBy User `gorm:"column:createdBy; not null"`
	//UpdatedBy User `gorm:"column:updatedBy; null"`
	//DeletedBy User `gorm:"column:deletedBy; null"`
	ID
	ComicsID     uint      `gorm:"column:comicsId; not null"`
	Image        string    `gorm:"size:191; column:image; not null"`
	PageNumber   uint      `gorm:"column:pageNumber; not null; DEFAULT:1"`
	TotalPage    uint      `gorm:"column:totalPage; not null; DEFAULT:1"`
	ReleasedYear time.Time `gorm:"column:releasedYear"`
	Model
}

func (ComicsPages) TableName() string {
	return "tbl_comics_pages"
}
