package models

type UserComics struct {
	//CreatedBy User `gorm:"column:createdBy; not null"`
	//UpdatedBy User `gorm:"column:updatedBy; null"`
	//DeletedBy User `gorm:"column:deletedBy; null"`
	ID
	ComicsID    uint   `gorm:"column:comicsId; not null"`
	ReadCounter uint   `gorm:"column:readCounter; not null; Default: 1"`
	Model
}

func (UserComics) TableName() string {
	return "tbl_user_comics"
}
