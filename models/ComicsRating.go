package models

type ComicsRating struct {
	//CreatedBy User `gorm:"column:createdBy; not null"`
	//UpdatedBy User `gorm:"column:updatedBy; null"`
	//DeletedBy User `gorm:"column:deletedBy; null"`
	ID
	ComicsID uint   `gorm:"column:comicsId; not null"`
	Rating   int    `gorm:"size:5; column:rating; not null; Default: 5"`
	Comments string `gorm:"size:191; column:comments"`
	Model
}

func (ComicsRating) TableName() string {
	return "tbl_comics_rating"
}
