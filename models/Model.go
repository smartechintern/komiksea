package models

import (
	"time"
)

type Model struct {
	CreatedBy time.Time  `gorm:"column:createdBy" json:"createdBy"`
	CreatedAt time.Time  `gorm:"column:createdAt" json:"createdAt"`
	UpdatedBy uint       `gorm:"column:updatedBy" json:"updatedBy"`
	UpdatedAt time.Time  `gorm:"column:updatedAt" json:"updatedAt"`
	DeletedBy uint       `gorm:"column:deletedBy" json:"deletedBy"`
	DeletedAt *time.Time `gorm:"column:deletedAt" json:"deletedAt"`
}
